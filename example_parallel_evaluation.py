from multiprocessing import Pool
import numpy as np

def my_func(mu):
    beta = np.array([[mu*0.5],[mu]]);
    return beta

if __name__ == '__main__':
    # Parameter to evaluate
    mu = np.linspace(0,10,100);

    # start 4 worker processes
    with Pool(processes=4) as pool:
        beta = [pool.apply_async(my_func, (i,)) for i in mu]
        print([res.get(timeout=1) for res in beta])

    # Acces to resuts
    A = beta[2].get();
    print(A);

    # Concatenate results in one array
    AA = np.empty([2,0]);
    for res in beta:
        AA = np.concatenate((AA,res.get()), axis=1);
    print(AA)

    # /!/ concatenation of results can be very inefficient

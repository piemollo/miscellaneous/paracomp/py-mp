from multiprocessing import Pool
import numpy as np

def my_func(mu):
    beta = np.array([[mu*0.5],[mu]]);
    return beta

if __name__ == '__main__':
    # Parameter to evaluate
    mu = range(10);
    
    # Where to store results
    beta = np.empty([2,0]);
    #beta = [];
    # start 4 worker processes
    with Pool(processes=4) as pool:

        # print same numbers in arbitrary order
        for i in pool.map(my_func, mu):
            beta = np.concatenate((beta,i),axis=1);

    print(beta)

    # /i/ pool.imap_unordered(.) for unordered parallelism

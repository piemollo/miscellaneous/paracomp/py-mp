import multiprocessing as mp
import os
os.environ["OMP_NUM_THREADS"] = "2"
os.environ["OPENBLAS_NUM_THREADS"] = "2"
os.environ["MKL_NUM_THREADS"] = "2"
os.environ["VECLIB_MAXIMUM_THREADS"] = "2"
os.environ["NUMEXPR_NUM_THREADS"] = "2"
import numpy as np
import timeit

def my_func(number):
    A = np.random.normal(0,1,[100,100]);
    A = A@A.transpose();
    b = np.random.normal(0,1,[100,1]);
    u = np.linalg.solve(A,b)
    return number

if __name__ == '__main__':
    N = 1000;

    start = timeit.default_timer()
    for i in range(N):
        my_func(i)
    stop = timeit.default_timer()
    print("Regular: %s " % (stop - start))

    start = timeit.default_timer()
    with mp.Pool(processes=4) as pool:
        pool.map(my_func, range(N))
    stop = timeit.default_timer()
    print("Pools: %s " % (stop - start))

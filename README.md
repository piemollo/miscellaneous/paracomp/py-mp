# Multi-Processing with Python

Cheat sheet for python multi-process.
Everything to know is in the 
[official doc](https://docs.python.org/3/library/multiprocessing.html).
The goal here is to provide ready-to-use examples and verify the actual
speedup using dummy benchmark.

## Issue with multi-processing and Numpy/Scipy

Some python libraries such as Numpy/Scipy use external routines.
Most common example is the use of Basic Linear Algebra Subprograms (BLAS),
which is by default OpenBLAS in linux.
These libraries are compiled to get the best performance relative to the 
hardware and they are multi-threaded.
Then, by default, they use all available resources, meaning that the default
number of *cores* used is the total number of cores in the computer.

The issue is the following: each process spawned by the multi-processing pool
of worker will copy this default behavior and request the maximal number of 
threads on the computer.
The total request of threads 
(_num. of process x num. of threads in the computer_) will widely exceed 
the number of actual threads, causing scheduling troubles and huge performances
drop.

To avoid that, one can control the maximal number of threads use by 
external routines locally.
Locally means here that *we do not need to recompile such libraries* or that 
changes are not permanent.
On linux, one easy and strong way to do that is to specify the limitation 
in an environment variable.
```shell
export OPENBLAS_NUM_THREADS=2
```
Another way to do that is to directly assign the variable in the python 
script.
```python
import os
os.environ["OPENBLAS_NUM_THREADS"] = 2
```

Other concerned libraries could be _OpenMP_, _MKL_, _VECLIB_, _NUMEXPR_, for 
example. Then the process is the same:
```python
import os
os.environ["OMP_NUM_THREADS"] = "2"
os.environ["OPENBLAS_NUM_THREADS"] = "2"
os.environ["MKL_NUM_THREADS"] = "2"
os.environ["VECLIB_MAXIMUM_THREADS"] = "2"
os.environ["NUMEXPR_NUM_THREADS"] = "2"
```

### Important remarks

* The environment variable modification must take place before the first
import of numpy/scipy
* In the example here, we set 2 threads maximum per process. This number 
must be chosen carefully depending on the number of process and the load
per process.

### Advanced remark
* Assigning load to threads via a scheduler is a powerful tool, in this
way it's possible to request any number of threads and the load will 
split between the actual number of physical threads 
(acting as a pool of threads). 
* This means that requesting slightly more threads than the actual 
number of threads available in the machine will reduce the 
sleeping time due to communication between them.
This can be used, to some extend, to maximize the workload.
[Source](https://stackoverflow.com/questions/15414027/multiprocessing-pool-makes-numpy-matrix-multiplication-slower).